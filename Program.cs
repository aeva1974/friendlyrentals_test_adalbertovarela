﻿using System;
using Autofac;
using FriendlyRentals.StringCalculator;

namespace fr_stringcalculator
{
    /// <summary>
    /// 
    /// </summary>
    class Program
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="args"></param>
        static void Main(string[] args)
        {
            Initialize();
            Console.Title = "Friendlyrentals string calculator kata";
            Console.WriteLine("Follow instructions on 'readme.md'");

            Console.WriteLine("CalculateWithEmptyStringReturnZero");
            var numbers = "0";
            var stringCalculator = new StringCalculator();
            Console.WriteLine(stringCalculator.Add(numbers));

            Console.WriteLine("CalculateWithOneNumberReturnSameNumber");
            numbers = "1";
            stringCalculator = new StringCalculator();

            Console.WriteLine(stringCalculator.Add(numbers));

            Console.WriteLine("CalculateWithTwoNumbersReturnTheirSum");
            numbers = "1,2";
            stringCalculator = new StringCalculator();

            Console.WriteLine(stringCalculator.Add(numbers));

            Console.WriteLine("CalculateWithAnyNumbersNumbersReturnTheirSum");
            numbers = "1,2,3,4,5";
            stringCalculator = new StringCalculator();

            Console.WriteLine(stringCalculator.Add(numbers));

            Console.WriteLine("CalculateUsingNewLineSeparatorsReturnTheirSum");
            numbers = string.Format("1{0}2,3{0}4", "\r\n");
            stringCalculator = new StringCalculator();

            Console.WriteLine(stringCalculator.Add(numbers));

            Console.WriteLine("CalculateUsingDifferentDelimitersReturnTheirSum");
            numbers = $"//;\r\n1;2;3;4";
            stringCalculator = new StringCalculator();

            Console.WriteLine(stringCalculator.Add(numbers));

            try
            {
                Console.WriteLine("CalculateWithANegativeNumberFails");
                numbers = "1,-2,3,4";
                stringCalculator = new StringCalculator();

                Console.WriteLine(stringCalculator.Add(numbers));
            }
            catch (Exception exception)
            {
                Console.WriteLine(exception.Message);
            }

            try
            {
                Console.WriteLine("CalculateWithNotANumberFails");
                numbers = "1,A";
                stringCalculator = new StringCalculator();

                Console.WriteLine(stringCalculator.Add(numbers));
            }
            catch (Exception exception)
            {
                Console.WriteLine(exception.Message);
            }
            Console.ReadLine();
        }

        /// <summary>
        /// 
        /// </summary>
        private static void Initialize()
        {
            var builder = new ContainerBuilder();

            builder.RegisterModule(new IoC.IoCModule());
        }
    }
}
