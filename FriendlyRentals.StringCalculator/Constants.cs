﻿namespace FriendlyRentals.StringCalculator
{
    /// <summary>
    /// 
    /// </summary>
    public static class Constants
    {
        /// <summary>
        /// 
        /// </summary>
        public const string DefaultDelimiter = ",|\r\n";

        /// <summary>
        /// 
        /// </summary>
        public const string StartDelimiter = "//";

        /// <summary>
        /// 
        /// </summary>
        public const string EndDelimiter = "\r\n";

        /// <summary>
        /// 
        /// </summary>
        public const int MinimumNumberToIncludeInSum = 0;
    }
}