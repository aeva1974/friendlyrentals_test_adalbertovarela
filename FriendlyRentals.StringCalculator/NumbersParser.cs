﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace FriendlyRentals.StringCalculator
{
    /// <summary>
    /// 
    /// </summary>
    public class NumbersParser : INumbersParser
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="inputSecuence"></param>
        /// <returns></returns>
        public IList<string> Parse(string inputSecuence)
        {
            var delimiters = Constants.DefaultDelimiter.Split('|');
            var secuenceWithoutDelimiters = inputSecuence;

            if (!inputSecuence.StartsWith(Constants.StartDelimiter))
                return secuenceWithoutDelimiters.Split(delimiters, StringSplitOptions.RemoveEmptyEntries);

            delimiters = ExtractDelimiters(inputSecuence);
            secuenceWithoutDelimiters = inputSecuence.Substring(inputSecuence.IndexOf(Constants.EndDelimiter, StringComparison.Ordinal) + Constants.EndDelimiter.Length);

            return secuenceWithoutDelimiters.Split(delimiters, StringSplitOptions.RemoveEmptyEntries);

        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="secuence"></param>
        /// <returns></returns>
        private static string[] ExtractDelimiters(string secuence)
        {
            var delimiterIndex = secuence.IndexOf(
                                     Constants.StartDelimiter,
                                     StringComparison.Ordinal) + Constants.StartDelimiter.Length;
            var numbersPart = secuence.Substring(
                delimiterIndex,
                secuence.IndexOf(Constants.EndDelimiter, StringComparison.Ordinal) - Constants.EndDelimiter.Length);

            var delimiters = new List<string> { numbersPart };

            return delimiters.ToArray();
        }
    }
}