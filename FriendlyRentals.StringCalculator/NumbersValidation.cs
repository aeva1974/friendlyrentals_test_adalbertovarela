﻿using System;
using System.Collections;
using System.Collections.Generic;

namespace FriendlyRentals.StringCalculator
{
    /// <summary>
    /// 
    /// </summary>
    public class NumbersValidation : INumbersValidation
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="parsedInput"></param>
        public void Validate(IEnumerable<string> parsedInput)
        {
            var negativeNumbers = new ArrayList();
            var alphabeticCharacters = new ArrayList();

            foreach (var number in parsedInput)
            {
                if (!int.TryParse(number.Trim(), out int numberInt))
                {
                    alphabeticCharacters.Add(number);
                }

                if (numberInt < Constants.MinimumNumberToIncludeInSum)
                {
                    negativeNumbers.Add(numberInt);
                }
            }

            if (alphabeticCharacters.Count > 0)
            {
                throw new Exception(
                    $"Alphabetic characters are not allowed: {string.Join(", ", alphabeticCharacters.ToArray())}");
            }

            if (negativeNumbers.Count > 0)
            {
                throw new Exception(
                    $"Negatives numbers are not allowed: {string.Join(", ", negativeNumbers.ToArray())}");
            }
        }
    }
}