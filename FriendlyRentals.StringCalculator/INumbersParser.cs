﻿using System.Collections.Generic;

namespace FriendlyRentals.StringCalculator
{
    /// <summary>
    /// 
    /// </summary>
    public interface INumbersParser
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="inputSecuence"></param>
        /// <returns></returns>
        IList<string> Parse(string inputSecuence);
    }
}