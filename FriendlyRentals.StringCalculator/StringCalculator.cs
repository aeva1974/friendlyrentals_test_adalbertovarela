﻿using FriendlyRentals.StringCalculator.Extensions;

namespace FriendlyRentals.StringCalculator
{
    /// <summary>
    /// 
    /// </summary>
    public class StringCalculator
    {
        /// <summary>
        /// 
        /// </summary>
        private readonly INumbersParser _numbersParser;

        /// <summary>
        /// 
        /// </summary>
        private readonly INumbersValidation _numbersValidation;

        /// <summary>
        /// 
        /// </summary>
        public StringCalculator()
        {
            _numbersParser = new NumbersParser();
            _numbersValidation = new NumbersValidation();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="numbersParser"></param>
        /// <param name="numbersValidation"></param>
        public StringCalculator(
            INumbersParser numbersParser,
            INumbersValidation numbersValidation)
        {
            _numbersParser = numbersParser;
            _numbersValidation = numbersValidation;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="inputSecuence"></param>
        /// <returns></returns>
        public int Add(string inputSecuence)
        {
            var parsedNumbers = _numbersParser.Parse(inputSecuence);

            _numbersValidation.Validate(parsedNumbers);

            return parsedNumbers.Sum();
        }
    }
}