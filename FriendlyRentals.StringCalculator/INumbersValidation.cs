﻿using System.Collections.Generic;

namespace FriendlyRentals.StringCalculator
{
    /// <summary>
    /// 
    /// </summary>
    public interface INumbersValidation
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="parsedInput"></param>
        void Validate(IEnumerable<string> parsedInput);
    }
}