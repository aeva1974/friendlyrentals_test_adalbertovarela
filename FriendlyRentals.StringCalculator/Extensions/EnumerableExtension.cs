﻿using System;
using System.Collections.Generic;
using static System.Int32;

namespace FriendlyRentals.StringCalculator.Extensions
{
    /// <summary>
    /// 
    /// </summary>
    public static class EnumerableExtension
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="numbers"></param>
        /// <returns></returns>
        public static int Sum(this IEnumerable<string> numbers)
        {
            var sum = 0;

            foreach (var number in numbers)
            {
                if (!TryParse(number.Trim(), out int numberInt))
                    throw new Exception("Incorrect number format!");

                sum += numberInt;
            }

            return sum;
        }
    }
}
