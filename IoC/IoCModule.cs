﻿using Autofac;
using FriendlyRentals.StringCalculator;

namespace fr_stringcalculator.IoC
{
    public class IoCModule : Module
    {
        protected override void Load(ContainerBuilder builder)
        {
            RegisterUtils(builder);
        }

        private static void RegisterUtils(ContainerBuilder builder)
        {
            builder.RegisterType<NumbersParser>().As<INumbersParser>();
            builder.RegisterType<NumbersValidation>().As<INumbersValidation>();
        }
    }
}