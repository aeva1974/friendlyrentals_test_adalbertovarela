﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace FriendlyRentals.StringCalculator.UnitTest
{
    [TestClass]
    public class StringCalculatorTest
    {
        [TestMethod]
        public void CalculateWithEmptyStringReturnZero()
        {
            const string numbers = "";
            var stringCalculator = new StringCalculator();

            Assert.AreEqual(0, stringCalculator.Add(numbers));
        }

        [TestMethod]
        public void CalculateWithOneNumberReturnSameNumber()
        {
            const string numbers = "1";
            var stringCalculator = new StringCalculator();

            Assert.AreEqual(1, stringCalculator.Add(numbers));
        }

        [TestMethod]
        public void CalculateWithTwoNumbersSuccess()
        {
            const string numbers = "1,2";
            var stringCalculator = new StringCalculator();

            stringCalculator.Add(numbers);

            Assert.IsTrue(true);
        }

        [TestMethod]
        public void CalculateWithTwoNumbersReturnTheirSum()
        {
            const string numbers = "1,2";
            var stringCalculator = new StringCalculator();

            Assert.AreEqual(1 + 2, stringCalculator.Add(numbers));
        }

        [TestMethod]
        public void CalculateWithAnyNumbersNumbersReturnTheirSum()
        {
            const string numbers = "1,2,3,4,5";
            var stringCalculator = new StringCalculator();

            Assert.AreEqual(1 + 2 + 3 + 4 + 5, stringCalculator.Add(numbers));
        }

        [TestMethod]
        public void CalculateUsingNewLineSeparatorsReturnTheirSum()
        {
            var numbers = string.Format("1{0}2,3{0}4", "\r\n");
            var stringCalculator = new StringCalculator();

            Assert.AreEqual(1 + 2 + 3 + 4, stringCalculator.Add(numbers));
        }

        [TestMethod]
        public void CalculateUsingDifferentDelimitersReturnTheirSum()
        {
            var numbers = $"//;\r\n1;2;3;4";
            var stringCalculator = new StringCalculator();

            Assert.AreEqual(1 + 2 + 3 + 4, stringCalculator.Add(numbers));
        }

        [TestMethod]
        public void CalculateWithANegativeNumberFails()
        {
            Exception exception = null;

            const string numbers = "1,-2,3,4";
            var stringCalculator = new StringCalculator();

            try
            {
                stringCalculator.Add(numbers);
            }
            catch (Exception e)
            {
                exception = e;
            }

            Assert.IsNotNull(exception);
            Assert.AreEqual("Negatives numbers are not allowed: -2", exception.Message);
        }

        [TestMethod]
        public void CalculateWithNotANumberFails()
        {
            Exception exception = null;

            const string numbers = "1,A";
            var stringCalculator = new StringCalculator();

            try
            {
                stringCalculator.Add(numbers);
            }
            catch (Exception e)
            {
                exception = e;
            }

            Assert.IsNotNull(exception);
            Assert.AreEqual("Alphabetic characters are not allowed: A", exception.Message);
        }
    }
}
